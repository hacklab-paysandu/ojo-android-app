package com.hacklab.ojo_android_app;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothSocket;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import java.io.IOException;
import java.util.List;
import java.util.Locale;
import java.util.Set;
import java.util.UUID;

public class Principal extends Activity
{
    private static final String TAG = "BT";

    private static final String BT_NAME = "HC-06";
    private static final UUID BT_UUID = UUID.fromString( "00001101-0000-1000-8000-00805F9B34FB" );
    private static final int BT_ACTIVAR = 1;

    private BluetoothAdapter btAdapter = null;
    private BluetoothSocket btSocket = null;
    private boolean conexionEstablecida = false;

    private boolean receverRegistrado = false;
    private final BroadcastReceiver receiver = new BroadcastReceiver ()
    {
        @Override
        public void onReceive ( Context context, Intent intent )
        {
            final String accion = intent.getAction();

            assert accion != null;
            if ( accion.equals( BluetoothDevice.ACTION_ACL_DISCONNECTED ) )
            {
                Toast.makeText(Principal.this, "Ojo desconectado", Toast.LENGTH_SHORT ).show();
                Log.e( TAG, "ACTION_ACL_DISCONNECTED" );
                bt_desconectar();
            }
            else if ( accion.equals( BluetoothAdapter.ACTION_STATE_CHANGED ) )
            {
                final int estado = intent.getIntExtra( BluetoothAdapter.EXTRA_STATE, BluetoothAdapter.ERROR );

                if ( estado == BluetoothAdapter.STATE_OFF )
                {
                    Log.e( TAG, "STATE_OFF" );
                    Toast.makeText(Principal.this, "Bluetooth apagado", Toast.LENGTH_SHORT ).show();
                    bt_desconectar();
                }
            }
        }
    };

    private ImageView backgroundBT, iconoBT, backgroundAgregar, backgroundManual;
    private View layoutEnfoque, layoutConfig, layoutNombre;
    private TextView tvConfig;

    @RequiresApi(api = Build.VERSION_CODES.O)
    @SuppressLint("DefaultLocale")
    @Override
    protected void onCreate ( Bundle savedInstanceState )
    {
        super.onCreate( savedInstanceState );
        setContentView( R.layout.activity_principal );

        backgroundBT = findViewById( R.id.backgroundBluetooth );
        iconoBT = findViewById( R.id.iconBluetooth );
        backgroundAgregar = findViewById( R.id.backgroundAgregar );
        backgroundManual = findViewById( R.id.backgroundManual );

        layoutEnfoque = findViewById( R.id.layoutEnfoques );
        layoutConfig = findViewById( R.id.layoutConfig );
        layoutNombre = findViewById( R.id.layoutNombre );

        tvConfig = findViewById( R.id.tituloConfig );

        findViewById( R.id.btnBluetooth ).setOnClickListener( new View.OnClickListener ()
        {
            @Override
            public void onClick ( View view )
            {
                if ( conexionEstablecida )
                    bt_desconectar();
                else
                    preguntar_para_conectar();
            }
        } );

        findViewById( R.id.btnManual ).setOnClickListener( new View.OnClickListener ()
        {
            @Override
            public void onClick ( View v )
            {
                layoutEnfoque.setVisibility( View.GONE );
                layoutConfig.setVisibility( View.VISIBLE );
                tvConfig.setText( getString( R.string.control_manual ) );

                backgroundManual.setColorFilter( getResources().getColor( R.color.bt_conectando ) );
                backgroundAgregar.setColorFilter( getResources().getColor( R.color.bt_conectado ) );

                layoutNombre.setVisibility( View.GONE );

                modo = MODO_MANUAL;
            }
        } );

        findViewById( R.id.btnAgregar ).setOnClickListener( new View.OnClickListener ()
        {
            @Override
            public void onClick ( View v )
            {
                layoutEnfoque.setVisibility( View.GONE );
                layoutConfig.setVisibility( View.VISIBLE );
                tvConfig.setText( getString( R.string.agregar_enfoque ) );

                backgroundAgregar.setColorFilter( getResources().getColor( R.color.bt_conectando ) );
                backgroundManual.setColorFilter( getResources().getColor( R.color.bt_conectado ) );

                layoutNombre.setVisibility( View.VISIBLE );

                modo = MODO_AGREGAR;
            }
        } );

        findViewById( R.id.btnAtras ).setOnClickListener( new View.OnClickListener ()
        {
            @Override
            public void onClick ( View v )
            {
                atras();
            }
        } );


        textViews = new TextView[CANT_SERVOS];

        textViews[0] = findViewById( R.id.tvNum1 );
        textViews[1] = findViewById( R.id.tvNum2 );
        textViews[2] = findViewById( R.id.tvNum3 );

        numeros = new int[CANT_SERVOS];

        for ( int i = 0; i < numeros.length; i++ )
            numeros[i] = 80;

        ( ( SeekBar ) findViewById( R.id.sbIris ) ).setMin( 10 );
        ( ( SeekBar ) findViewById( R.id.sbIris ) ).setMax( 170 );
        ( ( SeekBar ) findViewById( R.id.sbIris ) ).setProgress( 80 );
        ( ( SeekBar ) findViewById( R.id.sbIris ) ).setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser)
            {
                numeros[0] = progress;
                setTexto( 0 );
            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar)
            {
                bt_enviar( String.format( Locale.getDefault(), "servo:0:%d\n", 180 - numeros[0] ) );
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {}
        });

        ( ( SeekBar ) findViewById( R.id.sbCristalino ) ).setMin( 10 );
        ( ( SeekBar ) findViewById( R.id.sbCristalino ) ).setMax( 170 );
        ( ( SeekBar ) findViewById( R.id.sbCristalino ) ).setProgress( 80 );
        ( ( SeekBar ) findViewById( R.id.sbCristalino ) ).setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser)
            {
                numeros[1] = progress;
                setTexto( 1 );
            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar)
            {
                bt_enviar( String.format( Locale.getDefault(), "servo:1:%d\n", numeros[1] ) );
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {}
        });

        ( ( SeekBar ) findViewById( R.id.sbCamara ) ).setMin( 10 );
        ( ( SeekBar ) findViewById( R.id.sbCamara ) ).setMax( 170 );
        ( ( SeekBar ) findViewById( R.id.sbCamara ) ).setProgress( 80 );
        ( ( SeekBar ) findViewById( R.id.sbCamara ) ).setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser)
            {
                numeros[2] = progress;
                setTexto( 2 );
            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar)
            {
                bt_enviar( String.format( Locale.getDefault(), "servo:2:%d\n", numeros[2] ) );
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {}
        });

        setTexto( 0 );
        setTexto( 1 );
        setTexto( 2 );

        etNombreEnfoque = findViewById( R.id.etNombre );

        findViewById( R.id.btnSaveEnfoque ).setOnClickListener( new View.OnClickListener ()
        {
            @Override
            public void onClick ( View v )
            {
                String nombre = etNombreEnfoque.getText().toString().trim();

                if ( nombre.isEmpty() )
                    Toast.makeText(Principal.this, "Ingrese un nombre para el enfoque", Toast.LENGTH_SHORT ).show();
                else if ( BD.existeEnfoque( Principal.this, nombre ) )
                    Toast.makeText(Principal.this, String.format( "El nombre %s ya existe", nombre ), Toast.LENGTH_LONG ).show();
                else
                {
                    BD.agregarEnfoque( Principal.this, new Enfoque( nombre, numeros[0], numeros[1], numeros[2] ) );
                    Principal.this.ocultarTeclado( Principal.this );

                    numeros[0] = numeros[1] = numeros[2] = 80;

                    textViews[0].setText( String.format( "%d", numeros[0] ) );
                    textViews[1].setText( String.format( "%d", numeros[1] ) );
                    textViews[2].setText( String.format( "%d", numeros[2] ) );

                    etNombreEnfoque.setText( "" );

                    cargarEnfoques();
                    atras();
                }
            }
        } );

        rvEnfoques = findViewById( R.id.rvEnfoques );

        cargarEnfoques();
    }

    private float getReal ( int numero )
    {
        if ( numero == 0 )
            //return 5.01f * ( float ) Math.exp( 0.01f * numeros[0] );
            return 143.7981f + (6.14522f - 143.7981f)/(1f + (float)Math.pow((numeros[numero]/283.9528f),2.289726f));

        return numeros[numero];
    }

    private void setTexto ( int numero )
    {
        textViews[numero].setText( String.format( Locale.getDefault(), "~ %d mm", ( int ) getReal( numero ) ) );
    }

    @SuppressLint("DefaultLocale")
    public void enviar (Enfoque e )
    {
        if ( conexionEstablecida )
            bt_enviar( String.format( "todos:%d:%d:%d\n", 180 - e.getNum1(), e.getNum2(), e.getNum3() ) );
        else
        {
            Toast.makeText(Principal.this, "Establezca conexión con el ojo", Toast.LENGTH_SHORT ).show();
        }
    }

    @Override
    public void onBackPressed ()
    {
        if ( modo != 0 )
            atras();
        else
            super.onBackPressed();
    }

    private void ocultarTeclado (Activity a )
    {
        // https://stackoverflow.com/questions/1109022/close-hide-the-android-soft-keyboard
        InputMethodManager imm = ( InputMethodManager ) a.getSystemService( Activity.INPUT_METHOD_SERVICE );
        View v = a.getCurrentFocus();

        if ( v == null )
            v = new View( a );

        imm.hideSoftInputFromWindow( v.getWindowToken(), 0 );
    }

    private RecyclerView rvEnfoques;

    private void cargarEnfoques ()
    {
        List<Enfoque> enfoques = BD.listarEnfoques( this );

        if ( enfoques.size() == 0 )
            Log.e( TAG, "Vacio" );
        else
        {
            EnfoqueAdapter adapter = new EnfoqueAdapter(this, enfoques);
            rvEnfoques.setAdapter(adapter);
            rvEnfoques.setLayoutManager( new LinearLayoutManager( this ) );
        }
    }

    private void atras ()
    {
        layoutConfig.setVisibility( View.GONE );
        layoutEnfoque.setVisibility( View.VISIBLE );

        backgroundManual.setColorFilter( getResources().getColor( R.color.bt_conectado ) );
        backgroundAgregar.setColorFilter( getResources().getColor( R.color.bt_conectado ) );

        modo = 0;
    }

    private EditText etNombreEnfoque;

    private static final int CANT_SERVOS = 3;
    private static final int[] MINIMOS = new int[] { 0, 0, 0 };
    private static final int[] MAXIMOS = new int[] { 160, 160, 160 };

    private static final int MODO_MANUAL = 1;
    private static final int MODO_AGREGAR = 2;

    private int[] numeros;

    private int modo;

    private TextView[] textViews;

    @SuppressLint("DefaultLocale")
    private void flecha (int numero, int val, boolean add )
    {
        numero -= 1;

        if ( add )
            numeros[numero] += val;
        else
            numeros[numero] = val;

        if (numeros[numero] > MAXIMOS[numero])
            numeros[numero] = MAXIMOS[numero];
        else if (numeros[numero] < MINIMOS[numero])
            numeros[numero] = MINIMOS[numero];

        textViews[numero].setText( String.format( "%d", numeros[numero] ) );

        if ( modo == MODO_MANUAL )
        {
            if ( !conexionEstablecida )
            {
                Toast.makeText(Principal.this, "Establezca conexión con el ojo", Toast.LENGTH_LONG ).show();
                return;
            }
            bt_enviar( String.format( "servo:%d:%d\n", numero, traducir( numero, numeros[numero] ) ) );
        }
    }

    private int traducir ( int numero, int cant )
    {
        return cant + 10;
    }

    private void bt_enviar ( String mensaje )
    {
        Log.e( TAG, mensaje );
        try
        {
            if ( btSocket != null )
                btSocket.getOutputStream().write( mensaje.getBytes() );
        } catch ( IOException ignored ) {}
    }

    private void preguntar_para_conectar ()
    {
        btAdapter = BluetoothAdapter.getDefaultAdapter();

        if ( btAdapter == null )
            Toast.makeText( Principal.this, "Este dispositivo no cuenta con conexión bluetooth", Toast.LENGTH_LONG ).show();
        else if ( btAdapter.isEnabled() )
            bt_conectar();
        else
            new AlertDialog.Builder( Principal.this )
                .setIcon( android.R.drawable.ic_dialog_info )
                .setTitle( "Bluetooth" )
                .setMessage( "Es necesario encender el bluetooth" )
                .setPositiveButton("Ok", new DialogInterface.OnClickListener ()
                {
                    @Override
                    public void onClick ( DialogInterface dialogInterface, int i )
                    {
                        startActivityForResult( new Intent( BluetoothAdapter.ACTION_REQUEST_ENABLE ), BT_ACTIVAR );
                    }
                } )
                .show();
    }

    @Override
    protected void onActivityResult ( int requestCode, int resultCode, @Nullable Intent data )
    {
        super.onActivityResult( requestCode, resultCode, data );

        if ( requestCode == BT_ACTIVAR )
        {
            if ( resultCode == RESULT_OK )
                bt_conectar();
            else if ( resultCode == RESULT_CANCELED )
                Toast.makeText(Principal.this, "Es necesario encender el bluetooth", Toast.LENGTH_LONG ).show();
        }
    }

    @SuppressLint("StaticFieldLeak")
    private void bt_conectar ()
    {
        Set<BluetoothDevice> dispositivos = btAdapter.getBondedDevices();

        if ( dispositivos.size() == 0 )
            Toast.makeText(Principal.this, "Dispositivo " + BT_NAME + " no sincronizado", Toast.LENGTH_SHORT ).show();
        else
        {
            BluetoothDevice dispositivo = null;

            for ( BluetoothDevice disp : dispositivos )
                if ( disp.getName().equals( BT_NAME ) )
                {
                    dispositivo = disp;
                    break;
                }

            if ( dispositivo == null )
                Toast.makeText(Principal.this, "Dispositivo " + BT_NAME + " no sincronizado", Toast.LENGTH_SHORT ).show();
            else
            {
                new AsyncTask<BluetoothDevice, Void, Void> ()
                {
                    private boolean conectado = true;

                    @Override
                    protected void onPreExecute ()
                    {
                        backgroundBT.setColorFilter( getResources().getColor( R.color.bt_conectando ) );
                        iconoBT.setImageResource( R.drawable.ic_bluetooth_connecting );
                    }

                    @Override
                    protected Void doInBackground ( BluetoothDevice... dispositivo )
                    {
                        try
                        {
                            btSocket = dispositivo[0].createInsecureRfcommSocketToServiceRecord( BT_UUID );
                            btAdapter.cancelDiscovery();
                            btSocket.connect();
                        }
                        catch ( IOException e ) { conectado = false; }

                        return null;
                    }

                    @Override
                    protected void onPostExecute ( Void resultado )
                    {
                        if ( !conectado )
                        {
                            Toast.makeText(Principal.this, "No se puedo establecer la conexión con el ojo", Toast.LENGTH_SHORT).show();

                            backgroundBT.setColorFilter( getResources().getColor( R.color.bt_desconectado ) );
                            iconoBT.setImageResource( R.drawable.ic_bluetooth );
                        }
                        else
                            conexion_establecida();
                    }
                }.execute( dispositivo );
            }
        }
    }

    private void conexion_establecida ()
    {
        registerReceiver( receiver, new IntentFilter( BluetoothDevice.ACTION_ACL_DISCONNECTED ) );
        registerReceiver( receiver, new IntentFilter( BluetoothAdapter.ACTION_STATE_CHANGED ) );
        receverRegistrado = true;

        conexionEstablecida = true;
        backgroundBT.setColorFilter( getResources().getColor( R.color.bt_conectado ) );
        iconoBT.setImageResource( R.drawable.ic_bluetooth_connected );
    }

    private void bt_desconectar ()
    {
        Log.e( TAG, "bt_desconectar" );

        if ( receverRegistrado ) { unregisterReceiver( receiver ); receverRegistrado = false; }

        if ( btSocket != null )
        {
            try { btSocket.close(); }
            catch ( IOException ignored ) {}
        }

        conexionEstablecida = false;
        backgroundBT.setColorFilter( getResources().getColor( R.color.bt_desconectado ) );
        iconoBT.setImageResource( R.drawable.ic_bluetooth );
    }

    @Override
    protected void onDestroy ()
    {
        super.onDestroy();

        if ( receverRegistrado ) { unregisterReceiver( receiver ); receverRegistrado = false; }
        if ( conexionEstablecida ) bt_desconectar();
    }
}

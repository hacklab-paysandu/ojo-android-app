package com.hacklab.ojo_android_app;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.List;

public class EnfoqueAdapter extends RecyclerView.Adapter<EnfoqueAdapter.EnfoqueViewHolder>
{
    private Principal c;
    private List<Enfoque> enfoques;

    EnfoqueAdapter( Principal c, List<Enfoque> enfoques )
    {
        this.c = c;
        this.enfoques = enfoques;
    }

    private void enviar ( Enfoque e )
    {
        c.enviar( e );
    }

    private void quitar(final int p)
    {
        new AlertDialog.Builder( c )
                .setIcon( android.R.drawable.ic_dialog_info )
                .setTitle( "Borrar enfoque" )
                .setMessage( "¿Desea borrar el enfoque " + enfoques.get( p ).getNombre() + "?" )
                .setPositiveButton("Si", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick (DialogInterface dialog, int which )
                    {
                        BD.borrarEnfoque( c, enfoques.get( p ).getNombre() );

                        enfoques.remove( p );
                        notifyItemRemoved( p );

                        for ( int i = 0; i < enfoques.size(); i++ )
                            notifyItemChanged( i );
                    }
                } )
                .setNegativeButton( "No", null )
                .show();
    }

    @NonNull
    @Override
    public EnfoqueAdapter.EnfoqueViewHolder onCreateViewHolder ( @NonNull ViewGroup padre, int viewTipo )
    {
        View v = LayoutInflater.from( padre.getContext() ).inflate( R.layout.item_enfoque, padre, false );
        return new EnfoqueAdapter.EnfoqueViewHolder( v, this );
    }

    @Override
    public void onBindViewHolder ( @NonNull EnfoqueAdapter.EnfoqueViewHolder holder, int position )
    {
        holder.bindEnfoque( enfoques.get( position ), position );
    }

    @Override
    public int getItemCount ()
    {
        return enfoques.size();
    }

    class EnfoqueViewHolder extends RecyclerView.ViewHolder
    {
        private TextView tvNombre;
        private View btnEliminar;

        private View itemView;
        private EnfoqueAdapter ea;

        EnfoqueViewHolder (View itemView, final EnfoqueAdapter ea )
        {
            super( itemView );
            this.itemView = itemView;
            this.ea = ea;

            tvNombre = itemView.findViewById( R.id.tvNombre );
            btnEliminar = itemView.findViewById( R.id.btnEliminar );
        }

        void bindEnfoque ( final Enfoque e, final int position )
        {
            tvNombre.setText( e.getNombre() );

            itemView.setOnClickListener( new View.OnClickListener ()
            {
                @Override
                public void onClick ( View v )
                {
                    ea.enviar( e );
                }
            } );

            btnEliminar.setOnClickListener( new View.OnClickListener ()
            {
                @Override
                public void onClick ( View v )
                {
                    ea.quitar( position );
                }
            } );
        }
    }
}
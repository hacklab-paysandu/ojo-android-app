package com.hacklab.ojo_android_app;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import java.util.ArrayList;
import java.util.List;

public class BD extends SQLiteOpenHelper
{
    private static final int BD_VERSION = 10;
    private static final String BD_NOMBRE = "Ojo.bd";

    public BD ( Context c )
    {
        super( c, BD_NOMBRE, null, BD_VERSION );
    }

    public void onCreate ( SQLiteDatabase db )
    {
        db.execSQL( "CREATE TABLE enfoque ( nombre VARCHAR( 50 ), num1 INT, num2 INT, num3 INT );" );
    }

    public void onUpgrade ( SQLiteDatabase db, int vieja, int nueva )
    {
        db.delete( "enfoque", null, null );
    }

    public static void agregarEnfoque ( Context c, Enfoque e )
    {
        SQLiteDatabase db = new BD( c ).getWritableDatabase();

        ContentValues valores = new ContentValues();
        valores.put( "nombre", e.getNombre() );
        valores.put( "num1", e.getNum1() );
        valores.put( "num2", e.getNum2() );
        valores.put( "num3", e.getNum3() );

        db.insert( "enfoque", null, valores );

        db.close();
    }

    public static List<Enfoque> listarEnfoques ( Context c )
    {
        SQLiteDatabase db = new BD( c ).getWritableDatabase();

        Cursor cursor = db.rawQuery( "SELECT nombre, num1, num2, num3 FROM enfoque", null );

        List<Enfoque> res = new ArrayList<> ();
        while ( cursor.moveToNext() )
        {
            String nombre = cursor.getString( cursor.getColumnIndex( "nombre" ) );
            int num1 = cursor.getInt( cursor.getColumnIndex( "num1" ) );
            int num2 = cursor.getInt( cursor.getColumnIndex( "num2" ) );
            int num3 = cursor.getInt( cursor.getColumnIndex( "num3" ) );

            res.add( new Enfoque( nombre, num1, num2, num3 ) );
        }

        cursor.close();
        db.close();

        return res;
    }

    public static void borrarEnfoque ( Context c, String nombre )
    {
        SQLiteDatabase db = new BD( c ).getWritableDatabase();
        db.delete( "enfoque", "nombre = ?", new String[] { nombre } );
        db.close();
    }

    public static boolean existeEnfoque ( Context c, String nombre )
    {
        SQLiteDatabase db = new BD( c ).getWritableDatabase();

        Cursor cursor = db.rawQuery( "SELECT * FROM enfoque WHERE nombre = ?", new String[] { nombre } );
        boolean res = cursor.getCount() != 0;

        cursor.close();
        db.close();

        return res;
    }
}
